# [Delivery Service Full-Stack App](https://delivery-service-app-tau.vercel.app/)

## Overview

Delivery Service app is an advanced delivery management web application built using React for the frontend and Express.js for the backend. This project aims to provide users with a comprehensive solution for organizing shipping and boosting productivity.

## Features

### Frontend

-   **User Authentication**: Secure user authentication and authorization system to ensure data privacy and security.
-   **Shipping Management**: Create, edit, and delete shipments with ease. Organize them by categories, priority, due dates, and more.
-   **Roles**: Use roles to identify whether you are a driver or a shipper.
-   **Profile Customization**: Change name and a password!
	- Profile Image Customization -> (comming soon)

### Backend

-   **RESTful API**: Well-defined and documented RESTful API endpoints for seamless communication between frontend and backend.
-   **Database Integration**: Integration with a robust database system (e.g., MongoDB, PostgreSQL) for efficient data storage and retrieval.
-   **Authentication Middleware**: Implement authentication middleware to secure API endpoints and authenticate users.
-   **Scalability**: Design the backend system with scalability in mind to handle a growing user base and increased workload.

## Installation

1. Clone the repository: `git clone https://gitlab.com/oleksandryanov/delivery-service-app.git`
2. Navigate to the project directory: `cd delivery-service-app`
3. Install dependencies: `npm install`
4. Set up environmental variables: Create a `.env.development` files in the server and client directories and define the following variables: [TEMPLATE](.env.template)
5. Start: `npm run dev`

## Contributing

Contributions are welcome! If you have any suggestions, bug fixes, or new features to add, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

For any inquiries or feedback, please contact me via email: oleksandr.yanov.eu@gmail.com.
