import AuthContext from '@context/AuthContext';

import useAuth from '@hooks/auth.hook';

import Routes from './Routes';

const App = () => {
	const auth = useAuth();

	return (
		<AuthContext.Provider value={auth}>
			<Routes />
		</AuthContext.Provider>
	);
};

export default App;
