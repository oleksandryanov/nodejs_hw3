import { useContext, useEffect } from 'react';
import { Routes as RoutesWrapper, Route } from 'react-router-dom';

import { ROLES } from '@constants';

import AuthContext from '@context/AuthContext';

import useHttp from '@hooks/http.hook';

import RequireAuth from '@components/RequireAuth';
import Login from '@components/Login';
import Register from '@components/Register';
import Home from '@components/Home';
import Trucks from '@components/Trucks';
import Loads from '@components/Loads';
import ActiveLoad from '@components/ActiveLoad';
import LoadDetail from '@components/LoadDetail';
import Page404 from '@components/Page404';
import User from '@components/User';
import Spinner from '@components/Spinner';

import Layout from './Layout';

const Routes = () => {
	const auth = useContext(AuthContext);
	const { logout, loggedIn } = useHttp(true);

	useEffect(() => {
		loggedIn();
	}, []);

	if (!auth.isReady)
		return (
			<div className='loading-wrapper'>
				<Spinner />
			</div>
		);

	return (
		<RoutesWrapper>
			<Route path='/' element={<Layout />}>
				{/* Public */}
				<Route path='login' element={<Login />} />
				<Route path='register' element={<Register />} />

				{/* Private */}
				<Route element={<RequireAuth />}>
					<Route path='/' element={<Home logout={logout} />}>
						<Route
							element={
								<RequireAuth allowedRoles={[ROLES.driver]} />
							}
						>
							<Route path='/trucks' element={<Trucks />} />
						</Route>
						<Route path='/loads' element={<Loads />}>
							<Route
								element={
									<RequireAuth
										allowedRoles={[ROLES.driver]}
									/>
								}
							>
								<Route path='active' element={<ActiveLoad />} />
							</Route>
							<Route path=':id' element={<LoadDetail />} />
						</Route>
						<Route path='/users/me' element={<User />} />
					</Route>
				</Route>

				{/* Other */}
				<Route path='*' element={<Page404 />} />
			</Route>
		</RoutesWrapper>
	);
};

export default Routes;
