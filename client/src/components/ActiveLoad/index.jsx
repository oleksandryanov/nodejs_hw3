import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';

import classes from './index.module.scss';

import { ACTIVE_LOAD_URL } from '@constants';

import useHttp from '@hooks/http.hook';

import Spinner from '@components/Spinner';

const ActiveLoad = () => {
	const { loading, request } = useHttp(true);
	const [load, setLoad] = useState({});

	useEffect(() => {
		request({
			url: ACTIVE_LOAD_URL,
		})
			.then((res) => {
				setLoad(res.item);
			});
	}, [load?.state, request]);

	const changeState = async () => {
		try {
			const response = await request({
				url: `${ACTIVE_LOAD_URL}/state`,
				method: 'PATCH',
			});

			setLoad(response.item);

			toast.done(response.message);
		} catch (e) {
			toast.error(e);
		}
	};

	return (
		<section className={classes.active_load}>
			<h2>Active Load</h2>
			{loading ? (
				<Spinner />
			) : load.name ? (
				<>
					<p className={classes.name}>
						Name: <span>{load.name}</span>
					</p>
					<p className={classes.delivery}>
						Delivery address: <span>{load.delivery_address}</span>
					</p>
					<p className={classes.pickup}>
						Pickup address: <span>{load.pickup_address}</span>
					</p>
					<p className={classes.payload}>
						Payload: <span>{load.payload}</span>
					</p>
					<p className={classes.width}>
						Width: <span>{load.dimensions?.width}</span>
					</p>
					<p className={classes.height}>
						Height: <span>{load.dimensions?.height}</span>
					</p>
					<p className={classes.length}>
						Length: <span>{load.dimensions?.length}</span>
					</p>
					<p className={classes.date}>
						Created date:{' '}
						<span>
							{new Date(load.created_date).toLocaleDateString({
								day: 'numeric',
								month: 'short',
								year: 'numeric',
							})}
						</span>
					</p>
					<p className={classes.status}>
						Status: <span>{load.status}</span>
					</p>
					<p className={classes.state}>
						State: <span>{load.state}</span>
					</p>
					<div className={classes.logs}>
						{load.logs?.map((log) => (
							<div key={log._id}>
								<p>{log.message}</p>
								<p>
									{new Date(log.time).toLocaleDateString({
										day: 'numeric',
										month: 'short',
										year: 'numeric',
									})}
									||
									{new Date(log.time).toLocaleTimeString({
										hour: 'numeric',
										minute: 'numeric',
									})}
								</p>
							</div>
						))}
					</div>
					{load.status !== 'SHIPPED' && (
						<button
							className={classes.change_state}
							onClick={changeState}
						>
							Provide load state to next
						</button>
					)}
				</>
			) : (
				<p className={classes.none}>You don&apos;t have active load(</p>
			)}
		</section>
	);
};

export default ActiveLoad;
