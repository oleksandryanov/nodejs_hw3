import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import classes from './index.module.scss';

import Spinner from '@components/Spinner';

const Auth = ({
	children,
	onSubmit,
	loading,
	submitText,
	title,
	link,
	linkTitle,
	linkText,
}) => {
	return (
		<section className={classes.auth}>
			{loading ? (
				<Spinner />
			) : (
				<>
					<h1>{title}</h1>
					<form onSubmit={onSubmit}>
						{children}
						<input type='submit' value={submitText} />
					</form>
					<p className={classes.footer}>
						{linkTitle}
						<span>
							<Link to={link}>{linkText}</Link>
						</span>
					</p>
				</>
			)}
		</section>
	);
};

Auth.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
	onSubmit: PropTypes.func.isRequired,
	loading: PropTypes.bool.isRequired,
	submitText: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	link: PropTypes.string.isRequired,
	linkTitle: PropTypes.string.isRequired,
	linkText: PropTypes.string.isRequired,
};

export default Auth;
