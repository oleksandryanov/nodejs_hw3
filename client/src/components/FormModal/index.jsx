import PropTypes from 'prop-types';

import classes from './index.module.scss';

const FormModal = ({
	children,
	loading,
	isOpen,
	setOpen,
	submitText,
	onSubmit,
}) => {
	return (
		<div
			className={isOpen ? classes.modal : 'hide'}
			onClick={(e) =>
				e.target.className === 'form-modal' && setOpen(false)
			}
		>
			<div className={classes.modal__inner}>
				<form onSubmit={onSubmit}>
					{children}
					<input
						disabled={loading}
						type='submit'
						className={classes.submit}
						value={submitText}
					/>
				</form>
				<button
					disabled={loading}
					className={classes.close}
					onClick={() => setOpen(false)}
				>
					&#10060;
				</button>
			</div>
		</div>
	);
};

FormModal.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
	loading: PropTypes.bool.isRequired,
	setOpen: PropTypes.func.isRequired,
	isOpen: PropTypes.bool.isRequired,
	submitText: PropTypes.string.isRequired,
	onSubmit: PropTypes.func.isRequired,
};

export default FormModal;
