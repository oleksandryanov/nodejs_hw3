import { useContext, useState } from 'react';
import { Outlet, Link, useLocation } from 'react-router-dom';
import {
	ProSidebar,
	Menu,
	MenuItem,
	SidebarHeader,
	SidebarContent,
} from 'react-pro-sidebar';
import { Spin as Hamburger } from 'hamburger-react';
import { TbTruckDelivery } from 'react-icons/tb';
import { HiOutlineDownload } from 'react-icons/hi';
import { AiOutlineHome, AiOutlineUser } from 'react-icons/ai';
import { FaTruckLoading } from 'react-icons/fa';
import { MdOutlineLogout } from 'react-icons/md';

import 'react-pro-sidebar/dist/css/styles.css';
import classes from './index.module.scss';

import AuthContext from '@context/AuthContext';

import useHttp from '@hooks/http.hook';

const Home = () => {
	const auth = useContext(AuthContext);
	const { logout } = useHttp();

	const path = useLocation().pathname;

	const [isSidebarOpen, setSidebarOpen] = useState(false);

	return (
		<div className={classes.home}>
			<ProSidebar collapsed={!isSidebarOpen}>
				<SidebarHeader>
					<Hamburger
						size='40'
						toggled={isSidebarOpen}
						easing='ease-out'
						toggle={setSidebarOpen}
					/>
				</SidebarHeader>
				<SidebarContent>
					<Menu iconShape='square'>
						<MenuItem
							active={path === '/'}
							icon={<AiOutlineHome size='3.4rem' />}
						>
							Home
							<Link to='/' />
						</MenuItem>
						{auth.role === 'DRIVER' && (
							<>
								<MenuItem
									active={path === '/trucks'}
									icon={<TbTruckDelivery size='3.4rem' />}
								>
									Trucks
									<Link to='/trucks' />
								</MenuItem>
								<MenuItem
									active={path === '/loads/active'}
									icon={<FaTruckLoading size='3.4rem' />}
								>
									Active Load
									<Link to='/loads/active' />
								</MenuItem>
							</>
						)}
						<MenuItem
							active={
								path.includes('loads') &&
								path !== '/loads/active'
							}
							icon={<HiOutlineDownload size='3.4rem' />}
						>
							Loads
							<Link to='/loads' />
						</MenuItem>
						<MenuItem
							active={path === '/users/me'}
							icon={<AiOutlineUser size='3.4rem' />}
						>
							Profile
							<Link to='/users/me' />
						</MenuItem>
					</Menu>
				</SidebarContent>
			</ProSidebar>
			<div className={classes.wrapper}>
				<header>
					<Link to='/users/me' className={classes.profile}>
						<span>Hi {auth.name || auth.email}!</span>
						<div className={classes.profile__img}></div>
					</Link>
					<button className={classes.logout} onClick={logout}>
						<MdOutlineLogout size='3.4rem' />
					</button>
				</header>
				<main>
					{path === '/' ? (
						<div className={classes.info}></div>
					) : (
						<Outlet />
					)}
				</main>
			</div>
		</div>
	);
};

export default Home;
