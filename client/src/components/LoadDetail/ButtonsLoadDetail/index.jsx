import PropTypes from 'prop-types';

import classes from './index.module.scss';

const ButtonsLoadDetail = ({
	status,
	updateItem,
	deleteItem,
	postItem,
	isChanging,
	setChanging,
}) => {
	const render = () => {
		if (isChanging) {
			return (
				<>
					<button className={classes.save_info} onClick={updateItem}>
						Save load info
					</button>
					<button className={classes.delete} onClick={deleteItem}>
						Delete Load
					</button>
				</>
			);
		} else {
			return (
				<>
					<button
						className={classes.change_info}
						disabled={status === 'ASSIGNED'}
						onClick={() => setChanging(true)}
					>
						{status === 'ASSIGNED'
							? "You can't change load info"
							: 'Change load info'}
					</button>
					<button
						className={classes.post}
						disabled={status !== 'NEW'}
						onClick={postItem}
					>
						{status !== 'NEW' ? 'Already posted' : 'Post'}
					</button>
				</>
			);
		}
	};

	return <div className='btns'>{render()}</div>;
};

ButtonsLoadDetail.propTypes = {
	status: PropTypes.string.isRequired,
	updateItem: PropTypes.func.isRequired,
	deleteItem: PropTypes.func.isRequired,
	isChanging: PropTypes.bool.isRequired,
	setChanging: PropTypes.func.isRequired,
	postItem: PropTypes.func.isRequired,
};

export default ButtonsLoadDetail;
