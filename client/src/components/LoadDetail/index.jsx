import { useParams } from 'react-router-dom';
import { useContext, useEffect } from 'react';

import classes from './index.module.scss';

import {
	LOADS_URL,
	LOAD_DIMENSIONS,
	LOAD_NAME,
	LOAD_PAYLOAD,
} from '@constants';

import AuthContext from '@context/AuthContext';

import useItem from '@hooks/item.hook';

import Spinner from '@components/Spinner';

import ButtonsLoadDetail from './ButtonsLoadDetail';

const LoadDetail = () => {
	const auth = useContext(AuthContext);

	const _id = useParams().id;

	const initialState = {
		payload: LOAD_PAYLOAD,
		dimensions: LOAD_DIMENSIONS,
		name: LOAD_NAME,
		pickup_address: '',
		delivery_address: '',
		status: '',
		state: '',
		logs: [],
	};

	const {
		state,
		isChanging,
		loading,
		getItem,
		updateItem,
		deleteItem,
		postItem,
		renderItemLogs,
		dispatch,
		setChanging,
	} = useItem({
		initialState,
		_id,
		url: LOADS_URL,
		isLoadingByDefault: true,
	});

	useEffect(() => {
		getItem();
	}, [getItem]);

	return (
		<section className={classes.load_page}>
			{loading ? (
				<Spinner />
			) : (
				<>
					<div className={classes.info}>
						<label>
							Name:
							<input
								type='text'
								value={state.name}
								onChange={(e) =>
									dispatch({
										type: 'NEW_NAME',
										payload: e.target.value,
									})
								}
								readOnly={!isChanging}
							/>
						</label>
						<label>
							Payload:
							<input
								value={state.payload}
								onChange={(e) =>
									dispatch({
										type: 'NEW_PAYLOAD',
										payload: +e.target.value,
									})
								}
								type='number'
								min={1}
								readOnly={!isChanging}
							/>
						</label>
						<label>
							Width:
							<input
								value={state.dimensions.width}
								onChange={(e) =>
									dispatch({
										type: 'NEW_WIDTH',
										payload: +e.target.value,
									})
								}
								type='number'
								min={1}
								readOnly={!isChanging}
							/>
						</label>
						<label>
							Length:
							<input
								value={state.dimensions.length}
								onChange={(e) =>
									dispatch({
										type: 'NEW_LENGTH',
										payload: +e.target.value,
									})
								}
								type='number'
								min={1}
								readOnly={!isChanging}
							/>
						</label>
						<label>
							Height:
							<input
								value={state.dimensions.height}
								onChange={(e) =>
									dispatch({
										type: 'NEW_HEIGHT',
										payload: +e.target.value,
									})
								}
								type='number'
								min={1}
								readOnly={!isChanging}
							/>
						</label>
						<label>
							Pickup address:
							<span>{state.pickup_address}</span>
						</label>
						<label>
							Delivery address:
							<span>{state.delivery_address}</span>
						</label>
						<label>
							Status:
							<span
								className={
									classes[
										'status' +
											(state.status === 'NEW'
												? '_new'
												: '')
									]
								}
							>
								{state.status}
							</span>
						</label>
						{state.status !== 'NEW' && (
							<label>
								State:
								<span className={classes.state}>
									{state.state}
								</span>
							</label>
						)}
					</div>
					<div className={classes.logs}>{renderItemLogs()}</div>
					{auth.role === 'SHIPPER' && state.status !== 'SHIPPED' && (
						<ButtonsLoadDetail
							updateItem={updateItem}
							deleteItem={deleteItem}
							postItem={postItem}
							status={state.status}
							isChanging={isChanging}
							setChanging={setChanging}
						/>
					)}
				</>
			)}
		</section>
	);
};

export default LoadDetail;
