import { useState } from 'react';
import PropTypes from 'prop-types';

import classes from './index.module.scss';

import { LOAD_DIMENSIONS, LOAD_NAME, LOAD_PAYLOAD } from '@constants';

import FormModal from '@components/FormModal';

const FormModalAddLoad = ({ isOpen, setOpen, addItem, loading }) => {
	const [name, setName] = useState(LOAD_NAME);
	const [payload, setPayload] = useState(LOAD_PAYLOAD);
	const [dimensions, setDimensions] = useState(LOAD_DIMENSIONS);
	const [pickupAddress, setPickupAddress] = useState('');
	const [deliveryAddress, setDeliveryAddress] = useState('');

	const addLoad = async (e) => {
		e.preventDefault();

		addItem({
			name,
			payload,
			dimensions,
			pickup_address: pickupAddress,
			delivery_address: deliveryAddress,
		}).then(() => setOpen(false));
	};

	return (
		<FormModal
			loading={loading}
			isOpen={isOpen}
			setOpen={setOpen}
			onSubmit={addLoad}
			submitText='Add Load'
		>
			<label>
				Name:
				<input
					type='text'
					placeholder='Name'
					value={name}
					onChange={(e) => setName(e.target.value)}
				/>
			</label>
			<label>
				Payload:
				<input
					type='number'
					placeholder='Payload'
					value={payload}
					min={1}
					onChange={(e) => setPayload(e.target.value)}
				/>
			</label>
			<label>
				Pickup address:
				<input
					type='text'
					placeholder='Pickup address'
					value={pickupAddress}
					min={1}
					onChange={(e) => setPickupAddress(e.target.value)}
					required
				/>
			</label>
			<label>
				Delivery address:
				<input
					type='text'
					placeholder='Delivery address'
					value={deliveryAddress}
					min={1}
					onChange={(e) => setDeliveryAddress(e.target.value)}
					required
				/>
			</label>
			<label className={classes.dimensions}>
				Dimensions:
				<ul>
					<li>
						<label>
							Width:
							<input
								type='number'
								value={dimensions.width}
								onChange={(e) =>
									setDimensions({
										...dimensions,
										width: e.target.value,
									})
								}
								min={1}
							/>
						</label>
					</li>
					<li>
						<label>
							Height:
							<input
								type='number'
								value={dimensions.height}
								onChange={(e) =>
									setDimensions({
										...dimensions,
										height: e.target.value,
									})
								}
								min={1}
							/>
						</label>
					</li>
					<li>
						<label>
							Length:
							<input
								type='number'
								value={dimensions.length}
								onChange={(e) =>
									setDimensions({
										...dimensions,
										length: e.target.value,
									})
								}
								min={1}
							/>
						</label>
					</li>
				</ul>
			</label>
		</FormModal>
	);
};

FormModalAddLoad.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	setOpen: PropTypes.func.isRequired,
	addItem: PropTypes.func.isRequired,
	loading: PropTypes.bool.isRequired,
};

export default FormModalAddLoad;
