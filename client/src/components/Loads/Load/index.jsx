import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import classes from './index.module.scss';

const Load = ({ number, _id, name, status, state }) => {
	return (
		<Link to={`/loads/${_id}`} className={classes.item}>
			<>
				<h3>{number}</h3>
				<h3>Name: {name}</h3>
				<p>
					Status: <span>{status}</span>
				</p>
				{status === 'ASSIGNED' && (
					<p>
						State: <span>{state}</span>
					</p>
				)}
			</>
		</Link>
	);
};

Load.propTypes = {
	number: PropTypes.number.isRequired,
	_id: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	status: PropTypes.string.isRequired,
	state: PropTypes.string,
};

export default Load;
