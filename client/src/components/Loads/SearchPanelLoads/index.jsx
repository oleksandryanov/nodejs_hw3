import PropTypes from 'prop-types';

import classes from './index.module.scss';

import SearchPanel from '@components/SearchPanel';

const SearchPanelLoads = ({
	nameFilter,
	setNameFilter,
	statusFilter,
	setStatusFilter,
	role,
	setModalOpen,
}) => {
	return (
		<SearchPanel nameFilter={nameFilter} setNameFilter={setNameFilter}>
			<div className={classes.filters}>
				<button
					className={statusFilter === 'NEW' ? classes.active : ''}
					onClick={() =>
						setStatusFilter(statusFilter === 'NEW' ? '' : 'NEW')
					}
				>
					New
				</button>
				<button
					className={
						statusFilter === 'ASSIGNED' ? classes.active : ''
					}
					onClick={() =>
						setStatusFilter(
							statusFilter === 'ASSIGNED' ? '' : 'ASSIGNED'
						)
					}
				>
					Assigned
				</button>
				<button
					className={statusFilter === 'SHIPPED' ? classes.active : ''}
					onClick={() =>
						setStatusFilter(
							statusFilter === 'SHIPPED' ? '' : 'SHIPPED'
						)
					}
				>
					Shipped
				</button>
			</div>
			{role === 'SHIPPER' && (
				<button
					className={classes.add_btn}
					onClick={() => setModalOpen(true)}
				>
					Add Load
				</button>
			)}
		</SearchPanel>
	);
};

SearchPanelLoads.propTypes = {
	nameFilter: PropTypes.string.isRequired,
	setNameFilter: PropTypes.func.isRequired,
	statusFilter: PropTypes.string.isRequired,
	setStatusFilter: PropTypes.func.isRequired,
	role: PropTypes.string.isRequired,
	setModalOpen: PropTypes.func.isRequired,
};

export default SearchPanelLoads;
