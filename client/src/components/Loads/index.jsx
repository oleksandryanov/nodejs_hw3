import { useState, useContext } from 'react';
import { useLocation } from 'react-router-dom';
import { Outlet } from 'react-router-dom';

import classes from './index.module.scss';

import { LOADS_OFFSET, LOADS_URL } from '@constants';

import AuthContext from '@context/AuthContext';

import useData from '@hooks/data.hook';

import FormModalAddLoad from './FormModalAddLoad';
import SearchPanelLoads from './SearchPanelLoads';
import Load from './Load';

const Loads = () => {
	const location = useLocation();

	const { role } = useContext(AuthContext);

	const { loading, showLoadMore, renderData, addItem } = useData({
		baseOffset: LOADS_OFFSET,
		url: LOADS_URL,
	});

	const [isModalOpen, setModalOpen] = useState(false);

	const [nameFilter, setNameFilter] = useState('');
	const [statusFilter, setStatusFilter] = useState('');
	const [offset, setOffset] = useState({ offset: 0 });

	if (location.pathname !== '/loads') return <Outlet />;

	return (
		<section className={classes.wrapper}>
			<h2>Loads</h2>
			<SearchPanelLoads
				nameFilter={nameFilter}
				setNameFilter={setNameFilter}
				statusFilter={statusFilter}
				setStatusFilter={setStatusFilter}
				isModalOpen={isModalOpen}
				setModalOpen={setModalOpen}
				role={role}
			/>
			<div className={classes.items}>{renderData(Load)}</div>
			<button
				className={showLoadMore ? classes.load_more_btn : 'hide'}
				onClick={() => setOffset({ offset: offset.offset + 10 })}
			>
				Load more
			</button>
			<FormModalAddLoad
				addItem={addItem}
				loading={loading}
				isOpen={isModalOpen}
				setOpen={setModalOpen}
			/>
		</section>
	);
};

export default Loads;
