import { useRef, useState, useEffect, useContext } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';

import { LOGIN_URL } from '@constants';

import AuthContext from '@context/AuthContext';

import useHttp from '@hooks/http.hook';

import Auth from '@components/Auth';

const Login = () => {
	const { login, isLoggedIn } = useContext(AuthContext);
	const { loading, request } = useHttp();

	const navigate = useNavigate();
	const location = useLocation();
	const from = location.state?.from?.pathname || '/';

	const emailRef = useRef();

	const [email, setEmail] = useState('');
	const [pwd, setPwd] = useState('');

	useEffect(() => {
		if (isLoggedIn) {
			navigate('/', { replace: true });
		}

		emailRef.current.focus();
	}, [navigate, isLoggedIn]);

	const onLogin = async (e) => {
		e.preventDefault();

		try {
			const response = await request({
				url: LOGIN_URL,
				method: 'POST',
				body: JSON.stringify({ email, password: pwd }),
			});

			const role = response.role;
			const name = response.name;
			const receivedEmail = response.email;

			login(role, name, receivedEmail);
			setEmail('');
			setPwd('');

			toast.success(response.message);

			navigate(from, { replace: true });
		} catch (error) {
			toast.error(error);
		}
	};

	return (
		<Auth
			onSubmit={onLogin}
			loading={loading}
			submitText='Sign In'
			title='Login'
			link='/register'
			linkTitle='Need an Account?'
			linkText='Sign Up'
		>
			<label>
				Email:
				<input
					type='email'
					ref={emailRef}
					autoComplete='off'
					onChange={(e) => setEmail(e.target.value)}
					value={email}
					required
				/>
			</label>
			<label>
				Password:
				<input
					type='password'
					onChange={(e) => setPwd(e.target.value)}
					value={pwd}
					required
				/>
			</label>
		</Auth>
	);
};

export default Login;
