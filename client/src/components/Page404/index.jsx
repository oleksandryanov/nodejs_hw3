import { Link } from 'react-router-dom';

import classes from './index.module.scss';

const Page404 = () => {
	return (
		<article className={classes.page404}>
			<h1>Oops!</h1>
			<p>Page Not Found</p>
			<Link to='/'>Visit Our Homepage</Link>
		</article>
	);
};

export default Page404;
