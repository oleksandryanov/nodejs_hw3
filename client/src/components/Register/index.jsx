import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import classes from './index.module.scss';

import { REGISTER_URL } from '@constants';

import useHttp from '@hooks/http.hook';

import Auth from '@components/Auth';

const Register = () => {
	const { loading, request } = useHttp();

	const [email, setEmail] = useState('');
	const [emailFocus, setEmailFocus] = useState(false);
	const [validEmail, setValidEmail] = useState(false);

	const [pwd, setPwd] = useState('');
	const [pwdFocus, setPwdFocus] = useState(false);
	const [validPwd, setValidPwd] = useState(false);

	const [matchPwd, setMatchPwd] = useState('');
	const [matchFocus, setMatchFocus] = useState(false);
	const [validMatch, setValidMatch] = useState(false);

	const [role, setRole] = useState('SHIPPER');

	const navigate = useNavigate();

	useEffect(() => {
		setValidEmail(/\S+@\S+\.\S+/.test(email));
	}, [email]);

	useEffect(() => {
		setValidPwd(pwd.length >= 6);
		setValidMatch(pwd === matchPwd);
	}, [pwd, matchPwd]);

	const onRegister = async (e) => {
		e.preventDefault();

		if (!validEmail || !validMatch || !validPwd) {
			toast.error('Please specify params');
			return;
		}

		try {
			const response = await request({
				url: REGISTER_URL,
				method: 'POST',
				body: JSON.stringify({ email, password: pwd, role }),
			});

			setEmail('');
			setPwd('');
			setMatchPwd('');

			toast.success(response.message);

			navigate('/login');
		} catch (error) {
			toast.error(error);
		}
	};

	return (
		<Auth
			onSubmit={onRegister}
			loading={loading}
			submitText='Sign Up'
			title='Register'
			link='/login'
			linkTitle='Already registered?'
			linkText='Sign In'
		>
			<label>
				Email:
				<span
					id='emailnote'
					className={
						emailFocus && !validEmail ? classes.instruction : 'hide'
					}
				>
					Email must be an email
				</span>
				<input
					type='email'
					autoComplete='off'
					onChange={(e) => setEmail(e.target.value)}
					value={email}
					required
					aria-describedby='emailnote'
					onFocus={() => setEmailFocus(true)}
					onBlur={() => setEmailFocus(false)}
				/>
			</label>
			<label>
				Password:
				<span
					id='pwdnote'
					className={
						pwdFocus && !validPwd ? classes.instruction : 'hide'
					}
				>
					more then 6 characters.
				</span>
				<input
					type='password'
					autoComplete='off'
					onChange={(e) => setPwd(e.target.value)}
					value={pwd}
					required
					aria-describedby='pwdnote'
					onFocus={() => setPwdFocus(true)}
					onBlur={() => setPwdFocus(false)}
				/>
			</label>
			<label>
				Confirm Password:
				<span
					id='confirmnote'
					className={
						matchFocus && !validMatch ? classes.instruction : 'hide'
					}
				>
					Must match the first password input field.
				</span>
				<input
					type='password'
					autoComplete='off'
					onChange={(e) => setMatchPwd(e.target.value)}
					value={matchPwd}
					required
					aria-describedby='confirmnote'
					onFocus={() => setMatchFocus(true)}
					onBlur={() => setMatchFocus(false)}
				/>
			</label>
			<div className={classes.roles_wrapper}>
				<span>Role:</span>
				<div className={classes.roles}>
					<div
						onClick={() => setRole('DRIVER')}
						className={`${classes.role_btn} ${
							role === 'DRIVER' ? classes.active : ''
						}`}
					>
						Driver
					</div>
					<div
						onClick={() => setRole('SHIPPER')}
						className={`${classes.role_btn} ${
							role === 'SHIPPER' ? classes.active : ''
						}`}
					>
						Shipper
					</div>
				</div>
			</div>
		</Auth>
	);
};

export default Register;
