import { useLocation, Navigate, Outlet } from 'react-router-dom';
import { useContext } from 'react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

import AuthContext from '@context/AuthContext';

const RequireAuth = ({ allowedRoles }) => {
	const auth = useContext(AuthContext);
	const location = useLocation();

	const permissions = () => {
		toast.error("You dont't have permissions for this page(");
		return <Navigate to='/' state={{ from: location }} replace />;
	};

	const isUsersRoleAllowed = allowedRoles
		? allowedRoles.includes(auth.role)
		: true;

	const isUserAllowed = auth.isLoggedIn && isUsersRoleAllowed;

	return isUserAllowed ? (
		<Outlet />
	) : auth.isLoggedIn ? (
		permissions()
	) : (
		<Navigate to='/login' state={{ from: location }} replace />
	);
};

RequireAuth.propTypes = {
	allowedRoles: PropTypes.arrayOf(PropTypes.string),
};

export default RequireAuth;
