import PropTypes from 'prop-types';

import classes from './index.module.scss';

const SearchPanel = ({ children, nameFilter, setNameFilter }) => {
	return (
		<div className={classes.wrapper}>
			<input
				type='search'
				className={classes.search}
				name='search'
				value={nameFilter}
				onChange={(e) => setNameFilter(e.target.value)}
				autoComplete='false'
				autoCorrect='false'
			/>
			{children}
		</div>
	);
};

SearchPanel.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
	nameFilter: PropTypes.string.isRequired,
	setNameFilter: PropTypes.func.isRequired,
};

export default SearchPanel;
