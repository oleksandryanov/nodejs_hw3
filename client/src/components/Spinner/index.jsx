import classes from './index.module.scss';

export default function Spinner() {
	return (
		<div className={classes.spinner}>
			<div className={classes.loading}>
				<div className={classes.arc}></div>
				<div className={classes.arc}></div>
				<div className={classes.arc}></div>
			</div>
		</div>
	);
}
