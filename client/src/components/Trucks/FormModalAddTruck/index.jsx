import { useState } from 'react';
import PropTypes from 'prop-types';

import classes from './index.module.scss';

import { TRUCK_DIMENSIONS, TRUCK_NAME, TRUCK_PAYLOAD } from '@constants';

import FormModal from '@components/FormModal';

const FormModalAddTruck = ({ isOpen, setOpen, addItem, loading }) => {
	const [name, setName] = useState(TRUCK_NAME);
	const [payload, setPayload] = useState(TRUCK_PAYLOAD);
	const [dimensions, setDimensions] = useState(TRUCK_DIMENSIONS);

	const addTruck = (e) => {
		e.preventDefault();

		addItem({ name, payload, dimensions }).then(() => setOpen(false));
	};

	return (
		<FormModal
			loading={loading}
			isOpen={isOpen}
			setOpen={setOpen}
			onSubmit={addTruck}
			submitText='Add Truck'
		>
			<label>
				Name:
				<input
					type='text'
					placeholder={TRUCK_NAME}
					value={name}
					onChange={(e) => setName(e.target.value)}
				/>
			</label>
			<label>
				Payload:
				<input
					type='number'
					placeholder='Payload'
					value={payload}
					min={1}
					onChange={(e) => setPayload(e.target.value)}
				/>
			</label>
			<label className={classes.dimensions}>
				Dimensions:
				<ul>
					<li>
						<label>
							Width:
							<input
								type='number'
								value={dimensions.width}
								onChange={(e) =>
									setDimensions({
										...dimensions,
										width: e.target.value,
									})
								}
								min={1}
							/>
						</label>
					</li>
					<li>
						<label>
							Height:
							<input
								type='number'
								value={dimensions.height}
								onChange={(e) =>
									setDimensions({
										...dimensions,
										height: e.target.value,
									})
								}
								min={1}
							/>
						</label>
					</li>
					<li>
						<label>
							Length:
							<input
								type='number'
								value={dimensions.length}
								onChange={(e) =>
									setDimensions({
										...dimensions,
										length: e.target.value,
									})
								}
								min={1}
							/>
						</label>
					</li>
				</ul>
			</label>
		</FormModal>
	);
};

FormModalAddTruck.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	setOpen: PropTypes.func.isRequired,
	addItem: PropTypes.func.isRequired,
	loading: PropTypes.bool.isRequired,
};

export default FormModalAddTruck;
