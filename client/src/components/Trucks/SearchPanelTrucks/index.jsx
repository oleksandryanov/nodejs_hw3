import PropTypes from 'prop-types';

import classes from './index.module.scss';

import SearchPanel from '@components/SearchPanel';

const SearchPanelTrucks = ({
	nameFilter,
	setNameFilter,
	statusFilter,
	setStatusFilter,
	isFreeFilter,
	setFreeFilter,
	setModalOpen,
}) => {
	return (
		<SearchPanel nameFilter={nameFilter} setNameFilter={setNameFilter}>
			<div className={classes.filters}>
				<button
					className={statusFilter === 'IS' ? classes.active : ''}
					onClick={() =>
						setStatusFilter(statusFilter === 'IS' ? '' : 'IS')
					}
				>
					In Service
				</button>
				<button
					className={statusFilter === 'OL' ? classes.active : ''}
					onClick={() =>
						setStatusFilter(statusFilter === 'OL' ? '' : 'OL')
					}
				>
					On Load
				</button>
				<button
					className={`free ${isFreeFilter ? classes.active : ''}`}
					onClick={() => setFreeFilter(!isFreeFilter)}
				>
					Free
				</button>
			</div>
			<button
				className={classes.add_btn}
				onClick={() => setModalOpen(true)}
			>
				Add Truck
			</button>
		</SearchPanel>
	);
};

SearchPanelTrucks.propTypes = {
	nameFilter: PropTypes.string.isRequired,
	setNameFilter: PropTypes.func.isRequired,
	statusFilter: PropTypes.string.isRequired,
	setStatusFilter: PropTypes.func.isRequired,
	isFreeFilter: PropTypes.bool.isRequired,
	setFreeFilter: PropTypes.func.isRequired,
	setModalOpen: PropTypes.func.isRequired,
};

export default SearchPanelTrucks;
