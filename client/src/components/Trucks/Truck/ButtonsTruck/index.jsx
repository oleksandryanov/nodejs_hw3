import PropTypes from 'prop-types';

import classes from './index.module.scss';

const ButtonsTruck = ({
	status,
	updateItem,
	deleteItem,
	isChanging,
	setChanging,
	assigned_to,
	assignItem,
}) => {
	const render = () => {
		if (isChanging && status !== 'OL') {
			return (
				<>
					<button className={classes.save_info} onClick={updateItem}>
						Save truck info
					</button>
					<button className={classes.delete} onClick={deleteItem}>
						Delete truck
					</button>
				</>
			);
		} else if (status !== 'OL') {
			return (
				<>
					<button
						className={classes.change_info}
						onClick={() => setChanging(true)}
						disabled={status === 'OL'}
					>
						Change truck info
					</button>
					{status !== 'OL' && (
						<button
							className={classes.assign}
							disabled={!!assigned_to}
							onClick={() => !assigned_to && assignItem()}
						>
							{assigned_to ? 'Already assigned' : 'Assign'}
						</button>
					)}
				</>
			);
		}
	};

	return <div className='btns'>{render()}</div>;
};

ButtonsTruck.propTypes = {
	status: PropTypes.string.isRequired,
	updateItem: PropTypes.func.isRequired,
	deleteItem: PropTypes.func.isRequired,
	isChanging: PropTypes.bool.isRequired,
	setChanging: PropTypes.func.isRequired,
	assigned_to: PropTypes.string,
	assignItem: PropTypes.func.isRequired,
};

export default ButtonsTruck;
