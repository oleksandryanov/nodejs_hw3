import PropTypes from 'prop-types';

import classes from './index.module.scss';

import { TRUCKS_URL } from '@constants';

import useItem from '@hooks/item.hook';

import ButtonsTruck from './ButtonsTruck';

const Truck = ({
	number,
	_id,
	name,
	dimensions,
	payload,
	status,
	assigned_to,
	updateData,
	deleteData,
}) => {
	const initialState = {
		dimensions,
		payload,
		name,
	};

	const {
		state,
		isChanging,
		updateItem,
		dispatch,
		setChanging,
		deleteItem,
		assignItem,
	} = useItem({
		initialState,
		_id,
		deleteData,
		updateData,
		url: TRUCKS_URL,
	});

	return (
		<div className={classes.item}>
			<>
				<h3>{number}</h3>
				<label>
					Type:
					<input
						type='text'
						value={state.name}
						onChange={(e) =>
							dispatch({
								type: 'NEW_NAME',
								payload: e.target.value,
							})
						}
						readOnly={!isChanging}
					/>
				</label>
				<label>
					Payload:
					<input
						value={state.payload}
						onChange={(e) =>
							dispatch({
								type: 'NEW_PAYLOAD',
								payload: +e.target.value,
							})
						}
						type='number'
						min={1}
						readOnly={!isChanging}
					/>
				</label>
				<label>
					Width:
					<input
						value={state.dimensions.width}
						onChange={(e) =>
							dispatch({
								type: 'NEW_WIDTH',
								payload: +e.target.value,
							})
						}
						type='number'
						min={1}
						readOnly={!isChanging}
					/>
				</label>
				<label>
					Length:
					<input
						value={state.dimensions.length}
						onChange={(e) =>
							dispatch({
								type: 'NEW_LENGTH',
								payload: +e.target.value,
							})
						}
						type='number'
						min={1}
						readOnly={!isChanging}
					/>
				</label>
				<label>
					Height:
					<input
						value={state.dimensions.height}
						onChange={(e) =>
							dispatch({
								type: 'NEW_HEIGHT',
								payload: +e.target.value,
							})
						}
						type='number'
						min={1}
						readOnly={!isChanging}
					/>
				</label>
				<ButtonsTruck
					updateItem={updateItem}
					deleteItem={deleteItem}
					assignItem={assignItem}
					status={status}
					assigned_to={assigned_to}
					isChanging={isChanging}
					setChanging={setChanging}
				/>
			</>
		</div>
	);
};

Truck.propTypes = {
	_id: PropTypes.string.isRequired,
	number: PropTypes.number.isRequired,
	name: PropTypes.string.isRequired,
	dimensions: PropTypes.shape({
		width: PropTypes.number.isRequired,
		height: PropTypes.number.isRequired,
		length: PropTypes.number.isRequired,
	}),
	payload: PropTypes.number.isRequired,
	status: PropTypes.string.isRequired,
	assigned_to: PropTypes.string,
	updateData: PropTypes.func.isRequired,
	deleteData: PropTypes.func.isRequired,
};

export default Truck;
