import { useState } from 'react';

import classes from './index.module.scss';

import { TRUCKS_OFFSET, TRUCKS_URL } from '@constants';

import useData from '@hooks/data.hook';

import Truck from './Truck';
import SearchPanelTrucks from './SearchPanelTrucks';
import FormModalAddTruck from './FormModalAddTruck';

const Trucks = () => {
	const [isModalOpen, setModalOpen] = useState(false);

	const [nameFilter, setNameFilter] = useState('');
	const [statusFilter, setStatusFilter] = useState('');
	const [isFreeFilter, setFreeFilter] = useState(false);

	const { showLoadMore, loading, renderData, addItem, updateOffset } =
		useData({
			baseOffset: TRUCKS_OFFSET,
			url: TRUCKS_URL,
		});

	return (
		<section className={classes.wrapper}>
			<h2>Trucks</h2>
			<SearchPanelTrucks
				setModalOpen={setModalOpen}
				nameFilter={nameFilter}
				setNameFilter={setNameFilter}
				statusFilter={statusFilter}
				setStatusFilter={setStatusFilter}
				isFreeFilter={isFreeFilter}
				setFreeFilter={setFreeFilter}
			/>
			<div className={classes.items}>{renderData(Truck)}</div>
			<button
				className={showLoadMore ? classes.load_more_btn : 'hide'}
				onClick={() => updateOffset()}
			>
				Load more
			</button>
			<FormModalAddTruck
				addItem={addItem}
				loading={loading}
				isOpen={isModalOpen}
				setOpen={setModalOpen}
			/>
		</section>
	);
};

export default Trucks;
