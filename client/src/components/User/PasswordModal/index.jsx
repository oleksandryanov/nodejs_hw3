import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import classes from './index.module.scss';

import FormModal from '@components/FormModal';

const PasswordModal = ({ isOpen, setOpen, changePasswords, loading }) => {
	const [oldPassword, setOldPassword] = useState('');
	const [newPassword, setNewPassword] = useState('');
	const [isNewPasswordValid, setNewPasswordValid] = useState(false);

	useEffect(() => {
		setNewPasswordValid(newPassword.length >= 6);
	}, [newPassword]);

	const onChangePasswords = (e) => {
		e.preventDefault();

		changePasswords(oldPassword, newPassword, isNewPasswordValid).then(
			() => {
				setNewPassword('');
				setOldPassword('');
				setOpen(false);
			}
		);
	};

	return (
		<FormModal
			isOpen={isOpen}
			setOpen={setOpen}
			submitText='Change password'
			loading={loading}
			onSubmit={onChangePasswords}
		>
			<input
				type='password'
				placeholder='Old password'
				value={oldPassword}
				onChange={(e) => setOldPassword(e.target.value)}
				required
			/>
			<span
				id='pwdnote'
				className={!isNewPasswordValid ? classes.instruction : 'hide'}
			>
				more then 6 characters.
			</span>
			<input
				type='text'
				placeholder='New password'
				value={newPassword}
				onChange={(e) => setNewPassword(e.target.value)}
				required
			/>
		</FormModal>
	);
};

PasswordModal.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	setOpen: PropTypes.func.isRequired,
	changePasswords: PropTypes.func.isRequired,
	loading: PropTypes.bool.isRequired,
};

export default PasswordModal;
