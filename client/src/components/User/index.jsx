import { useState } from 'react';

import classes from './index.module.scss';

import useUser from '@hooks/user.hook';

import Spinner from '@components/Spinner';

import PasswordModal from './PasswordModal';

const User = () => {
	const [isModalOpen, setModalOpen] = useState(false);

	const {
		loading,
		name,
		setName,
		email,
		setEmail,
		role,
		isChanging,
		setChanging,
		saveInfo,
		changePasswords,
	} = useUser();

	return (
		<div className={classes.user}>
			{loading ? (
				<Spinner />
			) : (
				<>
					<div className={classes.credentials}>
						<div>
							Name:
							<input
								type='text'
								name='name'
								className={classes.name}
								value={
									name ||
									(!isChanging ? "You don't have a name" : '')
								}
								onChange={(e) => setName(e.target.value)}
								readOnly={!isChanging}
							/>
						</div>
						<div>
							Email:
							<input
								type='email'
								name='email'
								className={classes.email}
								value={
									email ||
									(!isChanging
										? "You don't have an email"
										: '')
								}
								onChange={(e) => setEmail(e.target.value)}
								readOnly={!isChanging}
							/>
						</div>
						<div className={classes.role}>
							Role:
							<span className={classes[role?.toLowerCase()]}>
								{role}
							</span>
						</div>
						<button
							className={classes.password_change_btn}
							onClick={() => setModalOpen(true)}
						>
							Change your password
						</button>
						{isChanging ? (
							<button
								className={`${classes.change_btn} ${classes.save_btn}`}
								onClick={saveInfo}
							>
								Save profile info
							</button>
						) : (
							<button
								className={classes.change_btn}
								onClick={() => setChanging(true)}
							>
								Change profile info
							</button>
						)}
					</div>
				</>
			)}
			<PasswordModal
				isOpen={isModalOpen}
				setOpen={setModalOpen}
				loading={loading}
				changePasswords={changePasswords}
			/>
		</div>
	);
};

export default User;
