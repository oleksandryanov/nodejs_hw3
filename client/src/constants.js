// URLs
export const URL = import.meta.env.VITE_BASE_URL + 'api/';
export const TRUCKS_URL = URL + 'trucks';
export const LOADS_URL = URL + 'loads';
export const ACTIVE_LOAD_URL = LOADS_URL + '/active';
export const AUTH_URL = URL + 'auth';
export const LOGOUT_URL = AUTH_URL + '/logout';
export const LOGIN_URL = AUTH_URL + '/login';
export const REGISTER_URL = AUTH_URL + '/register';
export const USER_URL = URL + 'users/me';
export const PASSWORD_URL = USER_URL + '/password';

// User roles
export const ROLES = {
	driver: 'DRIVER',
	shipper: 'SHIPPER',
};

// Base offset for the requests
export const TRUCKS_OFFSET = 10;
export const LOADS_OFFSET = 10;

// Base truck information
export const TRUCK_NAME = 'SPRINTER';
export const TRUCK_PAYLOAD = 1700;
export const TRUCK_DIMENSIONS = {
	width: 170,
	height: 250,
	length: 300,
};

// Base load information
export const LOAD_NAME = 'Delivery';
export const LOAD_PAYLOAD = 1;
export const LOAD_DIMENSIONS = {
	width: 1,
	height: 1,
	length: 1,
};
