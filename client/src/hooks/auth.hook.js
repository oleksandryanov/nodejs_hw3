import { useState } from 'react';
import Cookies from 'js-cookie';

const useAuth = () => {
	const [role, setRole] = useState(null);
	const [email, setEmail] = useState(null);
	const [name, setName] = useState(null);
	const [isLoggedIn, setLoggedIn] = useState(false);
	const [isReady, setReady] = useState(false);

	const login = (role, name, email) => {
		setLoggedIn(true);

		setUserData({ role, name, email });
	};

	const setUserData = ({ role, name, email }) => {
		setRole(role);
		setName(name);
		setEmail(email);

		Cookies.set('user_info', JSON.stringify({ role, name, email }));
	};

	const getUserData = () => {
		const data = Cookies.get()['user_info'];

		if (!data) return;

		const { role, name, email } = JSON.parse(data);

		setRole(role);
		setName(name);
		setEmail(email);
		setLoggedIn(true);
	};

	const clearStoredData = () => {
		setRole(null);
		setName(null);
		setEmail(null);
		Cookies.remove('user_info');
	};

	const logout = () => {
		clearStoredData();
		setLoggedIn(false);
	};

	return {
		login,
		logout,
		isLoggedIn,
		setLoggedIn,
		clearStoredData,
		getUserData,
		setUserData,
		role,
		email,
		name,
		isReady,
		setReady,
		setEmail,
		setName,
	};
};

export default useAuth;
