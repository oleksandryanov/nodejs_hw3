import { useState, useCallback, useEffect } from 'react';
import { toast } from 'react-toastify';

import useHttp from './http.hook';

import Spinner from '@components/Spinner';

const useData = ({ url, baseOffset }) => {
	const { request, loading, error } = useHttp(true);

	const [data, setData] = useState(null);
	const [showLoadMore, setShowLoadMore] = useState(false);

	const [offset, setOffset] = useState(0);

	const _filterByStatus = (_item, status) => {
		return _item.status === status;
	};

	const _filterByName = (_item, name) => {
		return _item.name.toLowerCase().indexOf(name.toLowerCase()) !== -1;
	};

	const _filterByIsFree = (_item) => {
		return !_item.assigned_to;
	};

	const resetOffset = () => setOffset(0);

	const updateOffset = () => setOffset((offset) => offset + 10);

	const addItem = async (item) => {
		try {
			const response = await request({
				method: 'POST',
				url: url,
				body: JSON.stringify(item),
			});

			addToData(response.item);
			toast.success(response.message);
		} catch (error) {
			toast.error(error);
			throw new Error(error);
		}
	};

	const loadData = useCallback(() => {
		request({
			url: `${url}?offset=${offset}`,
		})
			.then((res) => {
				setData((t) =>
					Array.isArray(t) && offset > 0
						? [...t, ...res.data]
						: res.data
				);

				if (res.data?.length < baseOffset) setShowLoadMore(false);
				else if (res.data?.length === baseOffset) setShowLoadMore(true);
			})
			.catch(toast.error);
	}, [offset, request, baseOffset, url]);

	useEffect(() => {
		loadData();
	}, [loadData]);

	const validateData = (status, name, isFree) => {
		if (!data) return;

		return data.filter((item) => {
			if (status && !_filterByStatus(item, status)) return false;
			if (name && !_filterByName(item, name)) return false;
			if (isFree && !_filterByIsFree(item)) return false;

			return true;
		});
	};

	const deleteData = (_id) => {
		setData((data) => data.filter((item) => item._id !== _id));
	};

	const updateData = (_id, payload) => {
		setData((data) =>
			data.map((item) =>
				item._id === _id ? { ...item, ...payload } : item
			)
		);
	};

	const addToData = (item) => {
		setData((data) => [...data, item]);
	};

	const renderData = (Component) => {
		if (loading) return <Spinner />;
		if (error) return <p>Error occured - {error}.</p>;

		const data = validateData();

		if (!data?.length) return <p className='none'>No data here(</p>;

		return data.map((item, index) => (
			<Component
				updateData={updateData}
				deleteData={deleteData}
				number={index + 1}
				key={item._id}
				{...item}
			/>
		));
	};

	return {
		showLoadMore,
		validateData,
		deleteData,
		updateData,
		addToData,
		addItem,
		loadData,
		renderData,
		resetOffset,
		updateOffset,
		data,
		loading,
		error,
	};
};

export default useData;
