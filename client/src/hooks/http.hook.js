import { useState, useCallback, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { LOGOUT_URL, AUTH_URL } from '@constants';

import AuthContext from '@context/AuthContext';

const useHttp = (isLoadingAtStart = false) => {
	const [loading, setLoading] = useState(isLoadingAtStart);
	const navigate = useNavigate();

	const auth = useContext(AuthContext);

	const logout = async () => {
		try {
			await request({
				url: LOGOUT_URL,
				method: 'POST',
			});

			auth.logout();
			navigate('/login', { replace: true });
		} catch (error) {
			toast.error(error?.message || error);
		}
	};

	const loggedIn = useCallback(async () => {
		try {
			await request({ url: AUTH_URL });

			auth.getUserData();
		} catch (error) {
			auth.setLoggedIn(false);
			auth.clearStoredData();
		} finally {
			auth.setReady(true);
		}
	}, []);

	const request = useCallback(
		async ({
			url,
			method = 'GET',
			body = null,
			headers = {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': 'no-cors',
			},
		}) => {
			setLoading(true);

			try {
				const response = await fetch(url, {
					mode: 'cors',
					credentials: 'include',
					method,
					body,
					headers,
				});
				const data = await response.json();

				if (!response.ok) {
					if (response.status === 403) {
						await logout();
					}
					throw data.errors || data.message;
				}
				setLoading(false);

				return data;
			} catch (e) {
				setLoading(false);
				throw Array.isArray(e) ? e.map((el) => el.msg).join('\n') : e;
			}
		},
		[],
	);

	return { loading, request, setLoading, logout, loggedIn };
};

export default useHttp;
