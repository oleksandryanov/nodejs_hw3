import { useReducer, useState, useRef, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import itemReducer from '@utils/itemReducer.util';

import useHttp from './http.hook';

const useItem = ({
	initialState,
	_id,
	deleteData,
	updateData,
	url,
	isLoadingByDefault = false,
}) => {
	const [state, dispatch] = useReducer(itemReducer, initialState);

	const [isChanging, setChanging] = useState(false);

	const navigate = useNavigate();

	const prevState = useRef(initialState);

	const { loading, request } = useHttp(isLoadingByDefault);

	const _isInfoUpdated = () =>
		JSON.stringify(prevState.current) !== JSON.stringify(state);

	const setPrevState = (newState) => {
		prevState.current = newState;
	};

	const getItem = useCallback(() => {
		request({
			url: `${url}/${_id}`,
		})
			.then(({ item }) => {
				dispatch({ type: 'NEW', payload: item });
				setPrevState(item);
			})
			.catch((error) => {
				toast.error(error);
				navigate(-1);
			});
	}, [request, _id, navigate, url]);

	const updateItem = () => {
		if (!_isInfoUpdated()) {
			setChanging(false);
			return;
		}

		request({
			method: 'PUT',
			url: `${url}/${_id}`,
			body: JSON.stringify(state),
		})
			.then(({ message }) => {
				toast.success(message);

				prevState.current = state;

				if (updateData) updateData(_id, state);

				setChanging(false);
			})
			.catch(toast.error);
	};

	const deleteItem = () => {
		request({
			method: 'DELETE',
			url: `${url}/${_id}`,
		})
			.then(() => {
				if (deleteData) deleteData(_id);

				toast.success('Deleted successfully!');
			})
			.catch(toast.error);
	};

	const assignItem = () => {
		request({
			method: 'POST',
			url: `${url}/${_id}/assign`,
		})
			.then(({ message, id }) => {
				if (updateData) updateData(_id, { assigned_to: id });

				toast.success(message);
			})
			.catch(toast.error);
	};

	const postItem = () => {
		request({
			method: 'POST',
			url: `${url}/${_id}/post`,
		})
			.then(({ message, driver_found }) => {
				if (!driver_found) {
					toast.info('Driver not found(');
				} else {
					toast.success(message);
				}
			})
			.catch(toast.error);
	};

	const renderItemLogs = () => {
		return state?.logs.map((log) => (
			<div className='log' key={log._id}>
				<p>{log.message}</p>
				<p>
					{new Date(log.time).toLocaleDateString({
						day: 'numeric',
						month: 'short',
						year: 'numeric',
					})}
					||
					{new Date(log.time).toLocaleTimeString({
						hour: 'numeric',
						minute: 'numeric',
					})}
				</p>
			</div>
		));
	};

	return {
		state,
		isChanging,
		loading,
		getItem,
		updateItem,
		deleteItem,
		assignItem,
		postItem,
		renderItemLogs,
		dispatch,
		setChanging,
		setPrevState,
	};
};

export default useItem;
