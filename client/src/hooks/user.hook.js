import { useContext, useState, useEffect, useRef, useCallback } from 'react';
import { toast } from 'react-toastify';

import { USER_URL, PASSWORD_URL } from '@constants';

import AuthContext from '@context/AuthContext';

import useHttp from '@hooks/http.hook';

const useUser = () => {
	const auth = useContext(AuthContext);

	const { loading, request } = useHttp(true);

	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [role, setRole] = useState('');

	const [isChanging, setChanging] = useState(false);

	const prevState = useRef({ name, email });

	const loadUser = useCallback(() => {
		request({ url: USER_URL })
			.then((data) => {
				setName(data.user.name || '');
				setEmail(data.user.email || '');
				setRole(data.user.role);

				prevState.current.name = data.user.name || '';
				prevState.current.email = data.user.email || '';
			})
			.catch(toast.error);
	}, [request]);

	useEffect(() => {
		loadUser();
	}, [loadUser]);

	const saveInfo = async () => {
		setChanging(false);
		
		if (
			(prevState.current.name === name &&
				prevState.current.email === email) ||
			(!name && !email) ||
			!email ||
			!/\S+@\S+\.\S+/.test(email)
		) {
			setName(prevState.current.name);
			setEmail(prevState.current.email);
			return;
		}

		try {
			const data = await request({
				method: 'PATCH',
				url: USER_URL,
				body: JSON.stringify({ email, name }),
			});

			prevState.current.name = data.user.name;
			prevState.current.email = data.user.email;

			auth.setUserData(data.user);
		} catch (error) {
			toast.error(error);
		}
	};

	const changePasswords = async (
		oldPassword,
		newPassword,
		isNewPasswordValid
	) => {
		if (!isNewPasswordValid) {
			toast.error('New password is invalid');
			return;
		}

		try {
			const { message } = await request({
				method: 'PATCH',
				url: PASSWORD_URL,
				body: JSON.stringify({ oldPassword, newPassword }),
			});

			toast.success(message);
		} catch (error) {
			toast.error(error);
		}
	};

	return {
		loadUser,
		saveInfo,
		changePasswords,
		name,
		setName,
		email,
		setEmail,
		loading,
		role,
		isChanging,
		setChanging
	};
};

export default useUser;
