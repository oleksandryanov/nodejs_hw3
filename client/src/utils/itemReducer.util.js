const itemReducer = (prevState, action) => {
	switch (action.type) {
		case 'NEW':
			return action.payload;

		case 'NEW_DIMENSIONS':
			return {
				...prevState,
				dimensions: action.payload,
			};

		case 'NEW_WIDTH':
			return {
				...prevState,
				dimensions: {
					...prevState.dimensions,
					width: action.payload,
				},
			};

		case 'NEW_HEIGHT':
			return {
				...prevState,
				dimensions: {
					...prevState.dimensions,
					height: action.payload,
				},
			};

		case 'NEW_LENGTH':
			return {
				...prevState,
				dimensions: {
					...prevState.dimensions,
					length: action.payload,
				},
			};

		case 'NEW_PAYLOAD':
			return {
				...prevState,
				payload: action.payload,
			};

		case 'NEW_TYPE':
			return {
				...prevState,
				type: action.payload,
			};

		case 'NEW_NAME':
			return {
				...prevState,
				name: action.payload,
			};

		case 'NEW_PICKUP_ADDRESS':
			return {
				...prevState,
				pickup_address: action.payload,
			};

		case 'NEW_DELIVERY_ADDRESS':
			return {
				...prevState,
				delivery_address: action.payload,
			};

		case 'NEW_STATUS':
			return {
				...prevState,
				status: action.payload,
			};

		case 'NEW_STATE':
			return {
				...prevState,
				state: action.payload,
			};

		case 'NEW_LOGS':
			return {
				...prevState,
				logs: action.payload,
			};

		default:
			return prevState;
	}
};

export default itemReducer;
