const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const { generate } = require('generate-password');
const nodemailer = require('nodemailer');

const generateJWT = require('../services/generateJWT');
const User = require('../models/User');

const register = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const { email, password, role } = req.body;

		const candidate = await User.findOne({ email });

		if (candidate) {
			return res.status(400).json({ message: 'User already exists' });
		}

		const hashedPassword = await bcrypt.hash(password, 12);
		const user = new User({ email, password: hashedPassword, role });

		await user.save();

		return res.status(200).json({ message: 'User created' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const login = async (req, res) => {
	try {
		const errors = validationResult(req);

		if (!errors.isEmpty()) {
			return res.status(400).json({
				errors: errors.array(),
				message: 'Incorrect data',
			});
		}

		const { email, password } = req.body;

		const user = await User.findOne({ email });

		if (!user) {
			return res.status(400).json({ message: 'User not found' });
		}

		const isMatch = await bcrypt.compare(password, user.password);

		if (!isMatch) {
			return res
				.status(400)
				.json({ message: 'Incorrect password, please try again' });
		}

		const token = generateJWT(user.email, user.id, user.role);

		return res
			.cookie('access_token', token, {
				domain: process.env.DOMAIN,
				httpOnly: true,
				secure: true,
				sameSite: 'None',
				maxAge: token.expiresIn,
			})
			.status(200)
			.json({
				message: 'Success',
				role: user.role,
				email: user.email,
				name: user.name,
			});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const logout = async (req, res) => {
	return res
		.clearCookie('access_token')
		.status(200)
		.json({ message: 'Logged out successfully' });
};

const getAuth = async (req, res) => {
	try {
		let token = req.cookies['access_token'];

		if (!token) throw new Error('Not authorized');

		return res.status(200).json({ message: 'Authorized' });
	} catch (e) {
		return res.status(401).json({ message: e.message });
	}
};

const forgotPassword = async (req, res) => {
	try {
		const user = await User.findOne({ email: req.body.email });

		if (!user) {
			return res.status(404).json({ message: 'User not found' });
		}

		const password = generate({ length: 8, numbers: true });

		const transporter = nodemailer.createTransport(
			{
				host: process.env.MAIL_HOST,
				port: process.env.MAIL,
				secure: false,
				auth: {
					user: process.env.MAIL_USER,
					pass: process.env.MAIL_PASSWORD,
				},
			},
			{
				from: process.env.MAIL_USER,
			}
		);

		await transporter.sendMail({
			from: process.env.MAIL_USER,
			to: user.email,
			subject: 'Your new password',
			text: `Your new password is: ${password}`,
		});

		user.password = await bcrypt.hash(password, 12);

		await user.save();

		return res.json({
			message: 'A new password has been sent to your email',
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

module.exports = { register, login, logout, getAuth, forgotPassword };
