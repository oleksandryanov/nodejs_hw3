const Truck = require('../models/Truck');
const User = require('../models/User');
const Load = require('../models/Load');

const loadStates = [
	'En route to Pick Up',
	'Arrived to Pick Up',
	'En route to delivery',
	'Arrived to delivery',
];

const getLoads = async (req, res) => {
	try {
		const loads = await Load.find({}, null, {
			limit: req.query.limit || 10,
			skip: req.query.offset || 0,
		});

		if (!loads) {
			return res.status(400).json({ message: 'No loads found' });
		}

		return res.json({ message: 'Success', data: loads });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const addLoad = async (req, res) => {
	try {
		const load = new Load({
			name: req.body.name,
			payload: req.body.payload,
			pickup_address: req.body.pickup_address,
			created_by: req.user.id,
			delivery_address: req.body.delivery_address,
			dimensions: req.body.dimensions,
			logs: [{ message: 'Load created successfully' }],
		});

		await load.save();

		return res.json({ message: 'Load created successfully', item: load });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const getActiveLoad = async (req, res) => {
	try {
		const user = await User.findById(req.user.id, 'assigned_to_load');
		const load = await Load.findById(user.assigned_to_load);

		if (!load) {
			return res.status(404).json({ message: 'Load not found' });
		}

		return res.json({ message: 'Success', item: load });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const changeLoadStateToNext = async (req, res) => {
	try {
		const user = await User.findById(req.user.id, 'assigned_to_load');
		const load = await Load.findById(user.assigned_to_load);

		if (!load) {
			return res.status(404).json({ message: 'Load not found' });
		}

		const currentState = loadStates.indexOf(load.state);

		if (load.state === loadStates[3]) {
			return res
				.status(400)
				.json({ message: 'Delivery has been finished' });
		}

		load.state = loadStates[currentState + 1] || loadStates[0];
		load.logs.push({ message: `Load state changed to ${load.state}` });

		if (load.state === loadStates[3]) {
			load.status = 'SHIPPED';
			load.logs.push({ message: 'Load delivery finished' });
		}

		await load.save();

		return res.json({
			message: `Load state changed to ${load.state}`,
			item: load,
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const getLoadById = async (req, res) => {
	try {
		const load = await Load.findById(req.params.id);

		if (!load) {
			return res.status(404).json({ message: 'Load not found' });
		}

		return res.json({ message: 'Success', item: load });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const updateLoadById = async (req, res) => {
	try {
		const load = await Load.findByIdAndUpdate(req.params.id, req.body);

		if (!load) {
			return res.status(404).json({ message: 'Load not found' });
		}

		load.logs.push({ message: 'Load info changed' });

		await load.save();

		return res.json({ message: 'Load details changed successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const deleteLoadById = async (req, res) => {
	try {
		const load = await Load.findById(req.params.id);

		if (!load) {
			return res.status(404).json({ message: 'Load not found' });
		}

		await load.delete();

		return res.json({ message: 'Load deleted successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const postLoadById = async (req, res) => {
	try {
		const load = await Load.findById(req.params.id);

		if (!load) {
			return res.status(404).json({ message: 'Load not found' });
		}

		if (load.status !== 'NEW') {
			return res
				.status(400)
				.json({ message: 'Load status has to be NEW' });
		}

		load.status = 'POSTED';

		await load.save();

		const truck = await Truck.findOne({
			status: 'IS',
			assigned_to: { $ne: null },
			payload: { $gte: load.payload },
			'dimensions.width': { $gte: load.dimensions.width },
			'dimensions.height': { $gte: load.dimensions.height },
			'dimensions.length': { $gte: load.dimensions.length },
		});

		if (truck) {
			truck.status = 'OL';
			load.status = 'ASSIGNED';
			load.assigned_to = truck.assigned_to;
			load.state = 'En route to Pick Up';
			load.logs.push({
				message: `Load assigned to driver with id ${truck.assigned_to}`,
			});

			await User.findByIdAndUpdate(load.assigned_to, {
				assigned_to_load: load.id,
			});

			await truck.save();
		} else {
			load.status = 'NEW';
			load.logs.push({ message: 'Truck for load not found' });
		}

		await load.save();

		return res.json({
			message: 'Load posted successfully',
			driver_found: !!truck,
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const getLoadInfoById = async (req, res) => {
	try {
		const load = await Load.findById(req.params.id);

		if (!load) {
			return res.status(404).json({ message: 'Load not found' });
		}

		if (load.status !== 'ASSIGNED') {
			return res
				.status(400)
				.json({ message: `Load status is ${load.status}` });
		}

		const truck = await Truck.findOne({ assigned_to: load.assigned_to });

		return res.json({ load, truck });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

module.exports = {
	getLoads,
	addLoad,
	getActiveLoad,
	changeLoadStateToNext,
	getLoadById,
	updateLoadById,
	deleteLoadById,
	postLoadById,
	getLoadInfoById,
};
