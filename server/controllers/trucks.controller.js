const Truck = require('../models/Truck');
const User = require('../models/User');

const getTrucks = async (req, res) => {
	try {
		const trucks = await Truck.find({ created_by: req.user.id }, null, {
			limit: req.query.limit || 10,
			skip: req.query.offset || 0,
		});

		if (!trucks) {
			return res.status(400).json({ message: 'No trucks found' });
		}

		return res.json({ message: 'Success', data: trucks });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const addTruck = async (req, res) => {
	try {
		const truck = new Truck({
			name: req.body.name,
			dimensions: req.body.dimensions,
			payload: req.body.payload,
			created_by: req.user.id,
		});

		await truck.save();

		return res.json({ message: 'Truck created successfully', item: truck });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const getTruckById = async (req, res) => {
	try {
		const truck = await Truck.findOne({
			created_by: req.user.id,
			_id: req.params.id,
		});

		if (!truck) {
			return res.status(404).json({ message: 'Truck not found' });
		}

		return res.json({ message: 'Success', item: truck });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const updateTruckById = async (req, res) => {
	try {
		const truck = await Truck.findOne({
			created_by: req.user.id,
			_id: req.params.id,
		});

		if (!truck) {
			return res.status(404).json({ message: 'Truck not found' });
		}

		truck.name = req.body.name || truck.name;
		truck.dimensions = req.body.dimensions || truck.dimensions;
		truck.payload = req.body.payload || truck.payload;

		truck.save();

		return res.json({ message: 'Truck details changed successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const deleteTruckById = async (req, res) => {
	try {
		const truck = await Truck.findOneAndDelete({
			created_by: req.user.id,
			_id: req.params.id,
		});

		if (!truck) {
			return res.status(404).json({ message: 'Truck not found' });
		}

		if (truck.assigned_to) {
			const user = await User.findById(truck.assigned_to);

			user.assigned_to = null;

			await user.save();
		}

		return res.json({ message: 'Truck deleted successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const assignTruckById = async (req, res) => {
	try {
		const truck = await Truck.findOne({
			created_by: req.user.id,
			_id: req.params.id,
		});
		const prevTruck = await Truck.findOne({
			created_by: req.user.id,
			assigned_to: req.user.id,
		});
		const user = await User.findById(req.user.id);

		if (!truck) {
			return res.status(404).json({ message: 'Truck not found' });
		}

		if (!user) {
			return res.status(404).json({ message: 'User not found' });
		}

		truck.assigned_to = user.id;
		user.assigned_to = truck.id;

		await truck.save();
		await user.save();

		if (prevTruck) {
			prevTruck.assigned_to = null;
			await prevTruck.save();
		}

		return res.json({
			message: 'Truck assigned successfully',
			id: truck.assigned_to,
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

module.exports = {
	getTrucks,
	addTruck,
	getTruckById,
	updateTruckById,
	deleteTruckById,
	assignTruckById,
};
