const bcrypt = require('bcryptjs');

const User = require('../models/User');

const getUser = async (req, res) => {
	try {
		const user = await User.findById(req.user.id);

		if (!user) {
			return res.status(400).json({ message: 'User not found' });
		}

		return res.json({
			user: {
				id: user.id,
				email: user.email,
				role: user.role,
				created_date: user.created_date,
				name: user.name,
			},
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const deleteUser = async (req, res) => {
	try {
		const user = await User.findById(req.user.id);

		if (!user) {
			return res.status(400).json({ message: 'User not found' });
		}

		await user.delete();

		return res.json({ message: 'Profile deleted successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const changeUserInfo = async (req, res) => {
	try {
		const user = await User.findById(req.user.id);

		if (!user) {
			return res.status(400).json({ message: 'User not found' });
		}

		user.name = req.body.name || user.name;
		user.email = req.body.email || user.email;

		await user.save();

		return res.json({
			message: 'User info changed successfully',
			user: { role: user.role, name: user.name, email: user.email },
		});
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const changeUserPassword = async (req, res) => {
	try {
		const user = await User.findById(req.user.id);

		if (!user) {
			return res.status(400).json({ message: 'User not found' });
		}

		if (!(await bcrypt.compare(req.body.oldPassword, user.password))) {
			return res.status(400).json({ message: 'Incorrect old password' });
		}

		user.password = await bcrypt.hash(req.body.newPassword, 12);

		await user.save();

		return res.json({ message: 'Password changed successfully' });
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

const getUserImg = async (req, res) => {
	try {
		const user = await User.findById(req.params.id);

		if (!user) {
			return res.status(400).json({ message: 'User not found' });
		}

		return res.send();
	} catch (e) {
		return res
			.status(500)
			.json({ message: 'Smth went wrong... Please try again' });
	}
};

module.exports = {
	getUser,
	deleteUser,
	changeUserPassword,
	getUserImg,
	changeUserInfo,
};
