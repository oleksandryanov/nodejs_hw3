const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
	try {
		const token = req.cookies.access_token;

		if (!token) {
			throw new Error('Not authorized');
		}

		const decoded = jwt.verify(token, process.env.JWT_SECRET);
		req.user = decoded;

		return next();
	} catch (e) {
		return res.status(403).json({ message: e.message });
	}
};
