module.exports = (req, res, next, role) => {
	if (req.user.role !== role) {
		return res
			.status(400)
			.json({ message: 'This resourse is not available for your role' });
	}

	return next();
};
