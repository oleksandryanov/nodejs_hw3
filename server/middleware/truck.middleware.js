module.exports = (req, res, next) => {
	switch (req.body.name) {
		case 'SPRINTER':
			req.body.payload = 1700;
			req.body.dimensions = {
				length: 300,
				height: 250,
				width: 170,
			};
			break;
		case 'SMALL STRAIGHT':
			req.body.payload = 2500;
			req.body.dimensions = {
				length: 500,
				height: 250,
				width: 170,
			};
			break;
		case 'LARGE STRAIGHT':
			req.body.payload = 4000;
			req.body.dimensions = {
				length: 700,
				height: 350,
				width: 200,
			};
			break;
		default:
			break;
	}

	next();
};
