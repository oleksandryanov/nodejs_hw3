const { Schema, model, Types } = require('mongoose');

const states = [
	'En route to Pick Up',
	'Arrived to Pick Up',
	'En route to delivery',
	'Arrived to delivery',
	null,
];

const schema = new Schema({
	name: { type: String, required: true },
	pickup_address: { type: String, required: true },
	delivery_address: { type: String, required: true },
	created_by: { type: Types.ObjectId, required: true, ref: 'User' },
	assigned_to: { type: Types.ObjectId, default: null, ref: 'User' },
	logs: [
		{
			message: { type: String, required: true },
			time: { type: Date, default: () => new Date() },
		},
	],
	status: {
		type: String,
		enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
		default: 'NEW',
	},
	state: { type: String, enum: states, default: null },
	dimensions: {
		width: { type: Number, required: true },
		length: { type: Number, required: true },
		height: { type: Number, required: true },
	},
	payload: { type: Number, required: true },
	created_date: { type: Date, default: () => new Date() },
});

module.exports = model('Load', schema);
