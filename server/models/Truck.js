const { Schema, model, Types } = require('mongoose');

const schema = new Schema({
	created_by: { type: Types.ObjectId, required: true, ref: 'User' },
	assigned_to: { type: Types.ObjectId, default: null, ref: 'User' },
	status: { type: String, enum: ['IS', 'OL'], default: 'IS' },
	name: { type: String, required: true },
	payload: { type: Number },
	dimensions: {
		width: { type: Number },
		length: { type: Number },
		height: { type: Number },
	},
	created_date: { type: Date, default: () => new Date() },
});

module.exports = model('Truck', schema);
