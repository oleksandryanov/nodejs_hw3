const { Schema, model, Types } = require('mongoose');

const schema = new Schema({
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	role: { type: String, required: true },
	assigned_to: { type: Types.ObjectId, ref: 'Truck' },
	assigned_to_load: { type: Types.ObjectId, ref: 'Load' },
	img: { type: Buffer },
	created_date: { type: Date, default: () => new Date() },
	name: { type: String },
});

module.exports = model('User', schema);
