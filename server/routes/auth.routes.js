const express = require('express');
const { check } = require('express-validator');

const router = express.Router();
const {
	register,
	login,
	forgotPassword,
	logout,
	getAuth,
} = require('../controllers/auth.controller');

router.get('/', getAuth);

router.post(
	'/register',
	[
		check('email', 'Incorrect email').isEmail(),
		check('password', 'Password have to be at least 6 symbols').isLength({
			min: 6,
		}),
		check('role', 'Incorrect role').isIn(['SHIPPER', 'DRIVER']),
	],
	register
);

router.post(
	'/login',
	[
		check('email', 'Incorrect email').isEmail(),
		check('password', 'Incorrect password').isLength({ min: 6 }),
	],
	login
);

router.post('/logout', logout);

router.post(
	'/forgot_password',
	[check('email', 'Incorrect email').isEmail()],
	forgotPassword
);

module.exports = router;
