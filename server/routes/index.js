const router = require('express').Router();

const authMiddleware = require('../middleware/auth.middleware');
const roleMiddleware = require('../middleware/role.middleware');

const authRoutes = require('./auth.routes');
const userRoutes = require('./user.routes');
const loadsRoutes = require('./loads.routes');
const trucksRoutes = require('./trucks.routes');

router.use('/users', authMiddleware, userRoutes);
router.use('/auth', authRoutes);
router.use('/loads', authMiddleware, loadsRoutes);
router.use(
	'/trucks',
	authMiddleware,
	(req, res, next) => roleMiddleware(req, res, next, 'DRIVER'),
	trucksRoutes
);

module.exports = router;
