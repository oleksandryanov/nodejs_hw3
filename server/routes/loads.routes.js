const express = require('express');
const { check } = require('express-validator');

const roleMiddleware = require('../middleware/role.middleware');

const {
	getLoads,
	addLoad,
	getActiveLoad,
	changeLoadStateToNext,
	getLoadById,
	updateLoadById,
	deleteLoadById,
	postLoadById,
	getLoadInfoById,
} = require('../controllers/loads.controller');

const router = express.Router();

router.get('/', getLoads);
router.post(
	'/',
	(req, res, next) => roleMiddleware(req, res, next, 'SHIPPER'),
	[
		check('name', 'Incorrect name').exists(),
		check('payload', 'Incorrect payload').isNumeric(),
		check('pickup_address', 'Incorrect pickup_address').exists(),
		check('delivery_address', 'Incorrect delivery_address').exists(),
		check('dimensions', 'Incorrect dimensions').isObject().exists(),
	],
	addLoad
);
router.get(
	'/active',
	(req, res, next) => roleMiddleware(req, res, next, 'DRIVER'),
	getActiveLoad
);
router.patch(
	'/active/state',
	(req, res, next) => roleMiddleware(req, res, next, 'DRIVER'),
	changeLoadStateToNext
);
router.get('/:id', getLoadById);
router.put(
	'/:id',
	(req, res, next) => roleMiddleware(req, res, next, 'SHIPPER'),
	updateLoadById
);
router.delete(
	'/:id',
	(req, res, next) => roleMiddleware(req, res, next, 'SHIPPER'),
	deleteLoadById
);
router.post(
	'/:id/post',
	(req, res, next) => roleMiddleware(req, res, next, 'SHIPPER'),
	postLoadById
);
router.get(
	'/:id/shipping_info',
	(req, res, next) => roleMiddleware(req, res, next, 'SHIPPER'),
	getLoadInfoById
);

module.exports = router;
