const express = require('express');

const truckMiddleware = require('../middleware/truck.middleware');

const router = express.Router();
const {
	getTrucks,
	addTruck,
	getTruckById,
	updateTruckById,
	deleteTruckById,
	assignTruckById,
} = require('../controllers/trucks.controller');

router.get('/', getTrucks);
router.post('/', truckMiddleware, addTruck);
router.get('/:id', getTruckById);
router.put('/:id', updateTruckById);
router.delete('/:id', deleteTruckById);
router.post('/:id/assign', assignTruckById);

module.exports = router;
