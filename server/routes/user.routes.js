const express = require('express');
const { check } = require('express-validator');

const router = express.Router();
const {
	getUser,
	deleteUser,
	changeUserPassword,
	getUserImg,
	changeUserInfo,
} = require('../controllers/user.controller');

router.get('/me', getUser);
router.delete('/me', deleteUser);
router.patch(
	'/me/password',
	[
		check(
			'oldPassword',
			'Old password have to be at least 6 symbols'
		).isLength({ min: 6 }),
		check(
			'newPassword',
			'New password have to be at least 6 symbols'
		).isLength({ min: 6 }),
		check(
			'newPassword',
			'Old and New passwords have to be different'
		).custom((value, { req }) => value !== req.body.oldPassword),
	],
	changeUserPassword
);
router.get('/:id/img', getUserImg);
router.patch('/me', changeUserInfo);

module.exports = router;
